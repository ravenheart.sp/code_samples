import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bem } from 'utils';
import { Menu, Dropdown, Button } from 'antd';
import { connect } from 'react-redux';
import { history } from 'redux/store';
import { startCase } from 'lodash';
import authActions from 'redux/auth/actions';
import accountActions from 'redux/account/actions';
import MAIN_ROUTE from 'constants/routes';
import TERMS from './constants';
import './style.scss';

const b = bem('header');
const { account } = MAIN_ROUTE;

const propTypes = {
  logout: PropTypes.func,
  id: PropTypes.string,
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  role: PropTypes.string,
  getAccount: PropTypes.func,
  company: PropTypes.shape({
    name: PropTypes.string,
  }),
};

const defaultProps = {
  logout: () => {},
  id: '',
  firstName: '',
  lastName: '',
  role: '',
  getAccount: () => {},
  company: {
    name: '',
  },
};

const {
  PROFILE,
  LOGOUT,
  ADMIN,
} = TERMS;

const menu = (logout, openProfile) => (
  <Menu>
    <Menu.Divider />
    <Menu.Item key="0">
      <Button
        className={b('btn')}
        type="text"
        onClick={() => openProfile()}
      >
        {PROFILE}
      </Button>

    </Menu.Item>
    <Menu.Item key="1">
      <Button
        className={b('btn')}
        type="text"
        onClick={() => logout()}
      >
        {LOGOUT}
      </Button>
    </Menu.Item>
  </Menu>
);

class Header extends Component {
  componentDidMount() {
    const { getAccount } = this.props;
    getAccount();
  }

  openProfile = () => {
    const { id } = this.props;
    history.push({
      pathname: `${account}/${id}`,
    });
  };

  render() {
    const {
      logout,
      firstName,
      lastName,
      role,
      company = {},
    } = this.props;
    const companyName = company && Object.keys(company).length && company.name ? company.name : '';
    const companyRole = role === ADMIN ? startCase(ADMIN) : companyName;
    return (
      <div className={b('main-wrapper')} id="header">
        <div className={b('logo')}> Fourth Zodiac </div>
        <Dropdown
          className={b('dropdown')}
          overlayClassName={b('overlay')}
          overlay={menu(logout, this.openProfile)}
          trigger={['click']}
          getPopupContainer={() => document.getElementById('header')}
        >
          <div>
            <div className={b('user-name')}>{`${firstName} ${lastName}`}</div>
            <div className={b('company-or-role')}>{companyRole}</div>
          </div>
        </Dropdown>
      </div>
    );
  }
}

Header.propTypes = propTypes;
Header.defaultProps = defaultProps;

const mapStateToProps = ({
  Auth: { role, id },
  account: {
    profile: {
      firstName,
      lastName,
      company,
    },
  },
}) => ({
  role,
  id,
  firstName,
  lastName,
  company,
});

export default connect(mapStateToProps, { ...authActions, ...accountActions })(Header);
