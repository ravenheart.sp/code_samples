import React from 'react';
import PropTypes from 'prop-types';
import { bem } from 'utils';
import { connect } from 'react-redux';
import { camelCase } from 'lodash';
import { history } from 'redux/store';
import MAIN_ROUTE from 'constants/routes';
import TERMS from './constants';
import MenuIcon from '../../svg/menuIcon';
import './style.scss';

const b = bem('sidebar');

const propTypes = {
  role: PropTypes.string,
  onTabSelect: PropTypes.func,
  currentTab: PropTypes.string,
};

const defaultProps = {
  role: '',
  onTabSelect: () => {},
  currentTab: '',
};

const {
  MANAGER,
  USER,
  MENU,
} = TERMS;

const Sidebar = (props) => {
  const getRoleDependingMenu = () => {
    const { role } = props;
    const menuList = [
      'Inventory',
      'Ship to FBA',
      'Ship retail',
      'Manufacturer inbound',
      'Users',
      'Companies',
    ];

    if (role === USER) {
      return menuList.slice(0, 4);
    }
    if (role === MANAGER) {
      return menuList.slice(0, 5);
    }
    return menuList;
  };

  const goToRoute = (path) => {
    history.push({
      pathname: path,
    });
  };

  const menu = getRoleDependingMenu();
  const { onTabSelect, currentTab } = props;

  return (
    <div className={b('main-wrapper')}>
      <div className={b('menu-header-wrapper')}>
        <div className={b('menu-header')}>
          <MenuIcon />
          <span className={b('menu-title')}>{MENU}</span>
        </div>
      </div>
      <div className={b('menu-items')}>
        {menu.map(item => (
          <div
            className={b('menu-item', { selected: camelCase(item) === currentTab })}
            role="button"
            tabIndex={-1}
            aria-hidden="true"
            onClick={
              currentTab
                ? () => onTabSelect(camelCase(item))
                : () => goToRoute(MAIN_ROUTE[camelCase(item)])
            }
          >
            {item}
          </div>
        ))}
      </div>
    </div>
  );
};

Sidebar.propTypes = propTypes;
Sidebar.defaultProps = defaultProps;

const mapStateToProps = ({ Auth: { role } }) => ({
  role,
});

export default connect(mapStateToProps, null)(Sidebar);
