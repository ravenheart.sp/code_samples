import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import actions from 'redux/inventory/actions';
import fbaActions from 'redux/shipToFba/actions';
import inboundActions from 'redux/manufacturerInbound/actions';
import { Table } from 'antd';
import { history } from 'redux/store';
import { bem } from 'utils';
import MAIN_ROUTE from 'constants/routes';
import columns from './config';
import './style.scss';

const { inventory } = MAIN_ROUTE;

const b = bem('general-packaging');

const propTypes = {
  isLoading: PropTypes.bool,
  generalPackaging: PropTypes.arrayOf(PropTypes.any),
  sorting: PropTypes.func,
  dimensionsOptions: PropTypes.arrayOf(PropTypes.shape({})),
  weightOptions: PropTypes.arrayOf(PropTypes.shape({})),
  convert: PropTypes.func,
  getSelectedInventoryId: PropTypes.func,
  getUsersDataShipToFba: PropTypes.func,
  getInventoryIdForInbound: PropTypes.func,
};

const defaultProps = {
  isLoading: false,
  generalPackaging: [],
  sorting: () => {},
  dimensionsOptions: [],
  weightOptions: [],
  convert: () => {},
  getSelectedInventoryId: () => {},
  getUsersDataShipToFba: () => {},
  getInventoryIdForInbound: () => {},
};
class General extends Component {
  componentDidMount() {
    this.checkSelection();
  }

  checkSelection = (selectedRowKeys, selectedRows = []) => {
    const {
      getSelectedInventoryId,
      getUsersDataShipToFba,
      getInventoryIdForInbound,
    } = this.props;
    const data = selectedRows && selectedRows.length ? selectedRows.map(({ id }) => id) : [];
    getSelectedInventoryId(data);
    getInventoryIdForInbound(data);
    const usersDataShipToFba = selectedRows.map(({
      id,
      name,
      value,
      sku,
      fnsku,
      quantity,
      newFnsku = '',
      length = '',
      width = '',
      height = '',
      weight = '',
    }) => ({
      id,
      name,
      value,
      sku,
      fnsku,
      quantity,
      newFnsku,
      length,
      width,
      height,
      weight,
    }));
    const dataForInbound = selectedRows.map(({
      id,
      name,
      inventoryImages,
      primaryId,
      primaryValue,
    }) => ({
      inventoryId: id,
      name,
      imageUrl: inventoryImages.length ? inventoryImages[0].imageUrl : null,
      primaryId,
      primaryValue,
    }));
    sessionStorage.dataForInbound = JSON.stringify(dataForInbound);
    sessionStorage.usersDataShipToFba = JSON.stringify(usersDataShipToFba);
    getUsersDataShipToFba(usersDataShipToFba);
  };

  goToRoute = (path, params) => {
    history.push({
      pathname: path,
      search: params,
    });
  };

  render() {
    const {
      isLoading,
      generalPackaging,
      sorting,
      dimensionsOptions,
      weightOptions,
      convert,
    } = this.props;

    const rowSelection = {
      onChange: this.checkSelection,
    };

    return (
      <div className={b()}>
        <Table
          className={b('table')}
          rowKey="id"
          dataSource={generalPackaging}
          rowSelection={rowSelection}
          loading={isLoading}
          columns={
              columns({
                b,
                sorting,
                dimensionsOptions,
                weightOptions,
                convert,
              })
            }
          onRow={record => ({
            onClick: () => this.goToRoute(`${inventory}/${record.id}`),
          })}
          pagination={false}
        />
      </div>
    );
  }
}

General.defaultProps = defaultProps;
General.propTypes = propTypes;

const mapStateToProps = ({ main, inventory: { isLoading } }) => (
  {
    ...main,
    isLoading,
  }
);

export default connect(
  mapStateToProps,
  { ...actions, ...fbaActions, ...inboundActions },
)(General);
